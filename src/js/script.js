$(document).ready(function () {

  svg4everybody();

  $('.s-page-header__catalog-button').on('click', function () {
    $('.s-page-header__catalog-menu').toggleClass('s-page-header__catalog-menu--is-open');
    $(this).toggleClass('s-page-header__catalog-button--is-active');
  });

  $('.s-hamburger').on('click', function () {
    $('.s-mobile-menu').addClass('s-mobile-menu--is-active');
    $('body').addClass('s-prevent-scroll');
  });

  $('.s-mobile-menu__close').on('click', function () {
    $('.s-mobile-menu').removeClass('s-mobile-menu--is-active');
    $('body').removeClass('s-prevent-scroll');
  });


  var rellax = new Rellax('.rellax');

  var swiper = new Swiper('.swiper-container', {
    autoHeight: true,
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 20,
        centeredSlides: true,
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          dynamicBullets: true
        },
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 20,
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          dynamicBullets: true
        },
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
          nextEl: '.s-recommended__slider-btn--next',
          prevEl: '.s-recommended__slider-btn--prev',
        }
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 30,
        navigation: {
          nextEl: '.s-recommended__slider-btn--next',
          prevEl: '.s-recommended__slider-btn--prev',
        }
      }
    }
  });

  $('.s-filter__mobile-toggler').on('click', function () {
    $('.s-filter__list').toggleClass('s-filter__list--is-open');
    $(this).toggleClass('s-filter__mobile-toggler--is-active');
  });
});